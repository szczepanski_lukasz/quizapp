package com.szczepanski.quizapp;

import org.parceler.Parcel;

@Parcel
class Answer {

    public String text;
    public boolean isCorrect;

    Answer() {
    }

    // konstruktor z tekstem i informacją, czy odpowiedź jest poprawna
    Answer(String text, boolean isCorrect) {
        this.text = text;
        this.isCorrect = isCorrect;
    }
}
