package com.szczepanski.quizapp;

import org.parceler.Parcel;

import java.util.ArrayList;

@Parcel
public class Quiz {

    public ArrayList<Question> questions = new ArrayList<>();
    public int allQuestions;
    public int CorrectAnswers;
    public int answersSoFar = 0;

}
