package com.szczepanski.quizapp;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import org.parceler.Parcels;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HighScoreActivity extends AppCompatActivity {

    @BindView(R.id.textViewResult)
    TextView textViewResult;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_high_score);
        ButterKnife.bind(this);
        SharedPreferences sharedPreferences = getSharedPreferences("pl.szczepanski.quizapp", MODE_PRIVATE);
        if (getIntent().hasExtra("quiz")){
            Quiz quiz = Parcels.unwrap(getIntent().getParcelableExtra("quiz"));
            if (quiz != null){
                textViewResult.setText(Integer.toString(quiz.CorrectAnswers));
                if (quiz.CorrectAnswers > sharedPreferences.getInt("highscore", 0)){
                    Toast.makeText(this,"Hey! New high score!", Toast.LENGTH_LONG).show();
                    sharedPreferences.edit().putInt("highscore", quiz.CorrectAnswers).apply();
                }
            }
        }
    }
}
