package com.szczepanski.quizapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import org.parceler.Parcels;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    // bind view dla biblioteki Butter Knife
    @BindView(R.id.RadioGreoup)
    RadioGroup radioGroup;
    @BindView(R.id.textViewQuestion)
    TextView textViewQuestion;

    Quiz quiz;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        if (getIntent().hasExtra("quiz")) {
            quiz = Parcels.unwrap(getIntent().getParcelableExtra("quiz"));
        } else {
            initQuiz();
        }

        setQuestion(quiz.questions.get(quiz.answersSoFar));
    }

    //ustawia aktualny tekst w pytaniu i odpowiedziach
    private void setQuestion(Question questionLocal) {
        textViewQuestion.setText(questionLocal.text);
        for (Answer answer : questionLocal.answers) {
            RadioButton radioButton = new RadioButton(this);
            radioButton.setText(answer.text);
            radioGroup.addView(radioButton);
        }
    }
    // zwraca indeks odpowiedzi  wybranej przez użytkownika,

    private int getSelectedIndex() {
        int radioButtonID = radioGroup.getCheckedRadioButtonId();
        View radioButton = radioGroup.findViewById(radioButtonID);
        return radioGroup.indexOfChild(radioButton);
    }
    // działanie przycisku przenoszącego do następnego pytania (Butter Knife)

    @OnClick(R.id.buttonNext)
    public void onClickNext() {
        if (getSelectedIndex() >= 0) {
            if (quiz.questions.get(quiz.answersSoFar).answers.get(getSelectedIndex()).isCorrect) {
                Toast.makeText(this, "CORRECT!", Toast.LENGTH_SHORT).show();
                quiz.CorrectAnswers++;
            } else {
                Toast.makeText(this, "NOT CORRECT!", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(this, "Select answer!", Toast.LENGTH_LONG).show();
            return;
        }

        //po sprawdzeniu poprawności pytania i wyświetleniu info dla uzytkownika
        //przejście do nowego ekranu z następnym pytaniem
        Intent intent;

        if (quiz.answersSoFar >= quiz.questions.size() - 1) {
            intent = new Intent(this, HighScoreActivity.class); //jeśli koniec pytań
        } else {
            intent = new Intent(this, MainActivity.class);
        }
        quiz.answersSoFar++;
        intent.putExtra("quiz", Parcels.wrap(quiz));
        startActivity(intent);
        this.finish();//nie można wrócić do poprzedniego pytania
    }

    //dla skrócenia ilości linijek w onCreate
    private void initQuiz() {
        quiz = new Quiz();
        Question question = new Question(getString(R.string.question_one));
        question.answers.add(new Answer(getString(R.string.answer_one), false));
        question.answers.add(new Answer(getString(R.string.answer_two), false));
        question.answers.add(new Answer(getString(R.string.answer_three), false));
        question.answers.add(new Answer(getString(R.string.answer_four), true));
        quiz.questions.add(question);

        question = new Question(getString(R.string.question_two));
        question.answers.add(new Answer(getString(R.string.answer_one_2), true));
        question.answers.add(new Answer(getString(R.string.answer_two_2), false));
        question.answers.add(new Answer(getString(R.string.answer_three_2), false));
        question.answers.add(new Answer(getString(R.string.answer_four_2), false));
        quiz.questions.add(question);

        question = new Question(getString(R.string.question_three));
        question.answers.add(new Answer(getString(R.string.answer_one_3), false));
        question.answers.add(new Answer(getString(R.string.answer_two_3), false));
        question.answers.add(new Answer(getString(R.string.answer_three_3), true));
        question.answers.add(new Answer(getString(R.string.answer_four_3), false));
        quiz.questions.add(question);

        question = new Question(getString(R.string.question_four));
        question.answers.add(new Answer(getString(R.string.answer_one_4), false));
        question.answers.add(new Answer(getString(R.string.answer_two_4), false));
        question.answers.add(new Answer(getString(R.string.answer_three_4), false));
        question.answers.add(new Answer(getString(R.string.answer_four_4), true));
        quiz.questions.add(question);

        question = new Question(getString(R.string.question_five));
        question.answers.add(new Answer(getString(R.string.answer_one_5), false));
        question.answers.add(new Answer(getString(R.string.answer_two_5), true));
        question.answers.add(new Answer(getString(R.string.answer_three_5), false));
        question.answers.add(new Answer(getString(R.string.answer_four_5), false));
        quiz.questions.add(question);

        question = new Question(getString(R.string.question_six));
        question.answers.add(new Answer(getString(R.string.answer_one_6), true));
        question.answers.add(new Answer(getString(R.string.answer_two_6), false));
        question.answers.add(new Answer(getString(R.string.answer_three_6), false));
        question.answers.add(new Answer(getString(R.string.answer_four_6), false));
        quiz.questions.add(question);

        question = new Question(getString(R.string.question_seven));
        question.answers.add(new Answer(getString(R.string.answer_one_7), false));
        question.answers.add(new Answer(getString(R.string.answer_two_7), true));
        question.answers.add(new Answer(getString(R.string.answer_three_7), false));
        question.answers.add(new Answer(getString(R.string.answer_four_7), false));
        quiz.questions.add(question);

        question = new Question(getString(R.string.question_eight));
        question.answers.add(new Answer(getString(R.string.answer_one_8), false));
        question.answers.add(new Answer(getString(R.string.answer_two_8), false));
        question.answers.add(new Answer(getString(R.string.answer_three_8), false));
        question.answers.add(new Answer(getString(R.string.answer_four_8), true));
        quiz.questions.add(question);
    }
}
