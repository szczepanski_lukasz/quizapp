package com.szczepanski.quizapp;

import org.parceler.Parcel;

import java.util.ArrayList;

@Parcel
public class Question {
    public String text;
    public ArrayList<Answer> answers = new ArrayList<>();

    Question() {
    }

    Question(String text) {
        this.text = text;
    }
}
